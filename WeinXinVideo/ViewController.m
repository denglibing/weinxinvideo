//
//  ViewController.m
//  WeinXinVideo
//
//  Created by Harry on 15/9/2.
//  Copyright (c) 2015年 Harry. All rights reserved.
//

#import "ViewController.h"
#import "WeiXinVideoViewController.h"

#import "WeiXinAnimateView.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
{
    UIImageView         *eyeIView;
    
    WeiXinAnimateView   *animateView;
    
    CADisplayLink       *displayLink;
    
    CGFloat             percent;
}

@property (weak, nonatomic) IBOutlet UITableView *friendTView;

@property (nonatomic, strong) WeiXinVideoViewController *wxVVC;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];
    
    percent = 0;
    
    UIImage *eyeImage = [UIImage imageNamed:@"icon_sight_capture_mask"];
    eyeIView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - eyeImage.size.width)/2,
                                                             20,
                                                             eyeImage.size.width,
                                                             eyeImage.size.height)];
    eyeIView.image = eyeImage;
    eyeIView.alpha = 0;
    [_friendTView.backgroundView addSubview:eyeIView];
    [self.view insertSubview:eyeIView belowSubview:_friendTView];
    
    animateView = [[WeiXinAnimateView alloc] initWithFrame:eyeIView.frame];
    animateView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:animateView belowSubview:_friendTView];
    
    
    animateView.coverColor = self.wxVVC.view.backgroundColor;
    [self.view insertSubview:self.wxVVC.view belowSubview:eyeIView];
    
    
    UIPanGestureRecognizer *upSwipe = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToShowFriendTView:)];
    upSwipe.delegate = self;
    [self.view addGestureRecognizer:upSwipe];
}


- (WeiXinVideoViewController *)wxVVC{
    if (!_wxVVC) {
        _wxVVC = [[WeiXinVideoViewController alloc] init];
    }
    return _wxVVC;
}

#pragma mark - tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"weixin_chat"];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY < -40) {
        percent = (- 40 - offsetY) / 60;
        [self updateVV];
    }

}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0){
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < -100) {
        
        eyeIView.alpha = 0;
        percent = 0;
        animateView.percent = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            _friendTView.frame = CGRectMake(0, _friendTView.frame.size.height, _friendTView.frame.size.width, _friendTView.frame.size.height);
        } completion:^(BOOL finished) {
            _friendTView.alpha = 1;
            
            [self.wxVVC addCaptureView];
        }];
    }
}


- (void)updateVV{
    
    eyeIView.alpha = percent;
    
    if (percent >= 1) {
        eyeIView.alpha = 1;
        animateView.percent = 1;
        [animateView setNeedsDisplay];
        return ;
    }
    
    animateView.percent = percent;
    [animateView setNeedsDisplay];
}


- (void)swipeToShowFriendTView:(UIPanGestureRecognizer *)panGes{
    
    CGPoint center = [panGes translationInView:self.view];    //获取点击self.view的中点
    
    [_friendTView setCenter:CGPointMake(_friendTView.center.x, center.y + _friendTView.center.y)];
    _friendTView.alpha = (_friendTView.frame.size.height - _friendTView.frame.origin.y / 2) / _friendTView.frame.size.height;
    
    //  注意一旦你完成上述的移动，将translation重置为0十分重要。否则translation每次都会叠加，很快你的view就会移除屏幕！
    [panGes setTranslation:CGPointMake(0, 0) inView:self.view];
    

    if(panGes.state == UIGestureRecognizerStateEnded) { // end slide.
        if (_friendTView.frame.origin.y < _friendTView.frame.size.height - 80) {
            [UIView animateWithDuration:0.4 animations:^{
                _friendTView.frame = CGRectMake(0, 0, _friendTView.frame.size.width, _friendTView.frame.size.height);
                _friendTView.alpha = 1;
                
                [self.wxVVC removeCatureView];
            }];
        }else{
            [UIView animateWithDuration:0.4 animations:^{
                _friendTView.frame = CGRectMake(0, _friendTView.frame.size.height, _friendTView.frame.size.width, _friendTView.frame.size.height);
                _friendTView.alpha = 0;
                
                [self.wxVVC addCaptureView];
            }];
        }
    }
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
