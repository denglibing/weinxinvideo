//
//  WeiXinVideoViewController.m
//  WeinXinVideo
//
//  Created by Harry on 15/9/2.
//  Copyright (c) 2015年 Harry. All rights reserved.
//

#import "WeiXinVideoViewController.h"

#import "CaptureViewController.h"

@interface WeiXinVideoViewController ()

@property (nonatomic, strong) CaptureViewController *captureVC;

@end

@implementation WeiXinVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];
}



- (CaptureViewController *)captureVC{
    if (!_captureVC) {
        _captureVC = [CaptureViewController new];
    }
    return _captureVC;
}


- (void)addCaptureView{
    [self.view addSubview:self.captureVC.view];
}

- (void)removeCatureView{
    [self.captureVC.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
